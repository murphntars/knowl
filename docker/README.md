# Docker Folder

This folder contains docker-compose files for setting up Knowl.

This will setup Knowl with as little input from the user as possible.

1. Download the docker-knowl folder using your preferred method.
2. Change the `host` constant in the file `../bookshelf/src/App.js` to localhost or the IP of the server running the stack.
3. Change any variables in the docker `.env` file if needed. NOTE: Change usernames and passwords in the .env file to secure your docker images! Knowl mount variables to the local directories containing the book and cover files.
4. Place the files `knowl-libgen-fiction.json` and `knowl-libgen-science.json` in the `./logstash/ingest` directory.
4. Run `docker-compose up -d | tee -a output.log` from this directory. 

#### That's it!
***
### Basic Steps performed: 
1. Downloads the libgen files if they are newer than what is already there.
2. Unrars the files.
3. Removed the depricated features from them
4. [not implemented yet] Pull newest compose and env files if not already present
5. Runs `docker-compose up -d` to bring all the containers up. 

At this point, you should be able to:
visit any database with: `docker exec -it <container-name> bash`
    <container-name> can be any of the containers made by docker-compose, including:
* bookwarrior_db (holds the bookwarrior database) _[MariaDB]_
* libfic_db (holds the libgen_fiction database) _[MariaDB]_

   For example: 
   ```
   $ docker exec -it knowl_bookwarrior_db bash
   $ mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD
   $ show databases;
   ```
The docker-compose also creates an 'adminer' instance which can be accessed, by default, on port 8080
***
#### Dev setup
My current dev setup is:
* Windows 10 Pro Build 19559.rs_prerelease.200131-1437
* WSL2, Docker Desktop setup for WSL2, passing the socket
* WSL2 - Ubuntu 18.04.4

I run everything in the WSL2 instance. 

I code everything using VS Code Insiders. To do so, simply:
* Open VS Code
* Select 'Open Folder'
* In the address bar in the Explorer window, enter `\\wsl$\Ubuntu-18.04\home\<usernam>` where <username> is your WSL2 username. This will open your WSL2 Linux home folder in VS Code. 
* Set your default shell to your WSL2 instance.

Alternatively, install the 'Remote - WSL' extension in VS Code. ('Remote - WSL' runs commands and extensions directly in WSL so you don't have to worry about pathing issues, binary compatibility, or other cross-OS challenges. You're able to use VS Code in WSL just as you would from Windows.)

#####NOTE:
Using git with Remote-WSL can get odd. I don't have that setup. I use a different git application.
